const Towxml = require('/towxml/main'); 
App({
  towxml: new Towxml(),
  replyTime(time) {
    var yearIndex = time.indexOf('-');
    var year = time.substr(0, yearIndex);
    var month = time.substr(yearIndex + 1, 2);
    var day = time.substr(yearIndex + 4, 2);
    var hourIndex = time.indexOf('T');
    var hour = time.substr(hourIndex + 1, 2);
    var min = time.substr(hourIndex + 4, 2);
    var sec = time.substr(hourIndex + 7, 2);
    return { year: year, month: month, day: day, hour: hour, min: min, sec: sec }
  },
  getTime() {
    var time = new Date();
    var year = time.getFullYear();
    var month = time.getMonth() + 1;
    var day = time.getDate();
    var hour = time.getHours();
    var min = time.getMinutes();
    var sec = time.getSeconds();
    if (month.toString().length == 1) {
      month = "0" + month;
    }
    if (day.toString().length == 1) {
      day = "0" + day;
    }
    if (hour.toString().length == 1) {
      hour = "0" + hour;
    }
    if (min.toString().length == 1) {
      min = "0" + min;
    }
    if (sec.toString().length == 1) {
      sec = "0" + sec;
    }
    return { year: year, month: month, day: day, hour: hour, min: min, sec: sec };
  },
  filter(reply,now){
    if(reply.year == now.year){
      if (reply.month == now.month){
        if (reply.day == now.day){
          if (reply.hour == now.hour){
            if (reply.min == now.min) {
              if (reply.sec == now.sec) {
                return "刚刚"
              }else{
                
              }
            }
          }
        }
      }
    }
  },
  /**
   * 当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
   */
  onLaunch: function () {

  },

  /**
   * 当小程序启动，或从后台进入前台显示，会触发 onShow
   */
  onShow: function (options) {

  },

  /**
   * 当小程序从前台进入后台，会触发 onHide
   */
  onHide: function () {

  },

  /**
   * 当小程序发生脚本错误，或者 api 调用失败时，会触发 onError 并带上错误信息
   */
  onError: function (msg) {

  }
})
