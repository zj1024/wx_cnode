const api = 'https://cnodejs.org/api/v1';
const util = require('../../utils/util.js');
const app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    list: [
      { content: "全部", ischecked: false, tar: '' },
      { content: "精华", ischecked: false, tar: 'good' },
      { content: "分享", ischecked: false, tar: 'share' },
      { content: "问答", ischecked: false, tar: 'ask' },
      { content: "招聘", ischecked: false, tar: 'job' }
    ],
    dataList: []
  },
  checked(e) {
    wx.showLoading({
      title:"正在加载..."
    })
    var that = this;
    for (var i = 0; i < this.data.list.length; i++) {
      that.setData({
        ['list[' + i + '].ischecked']: false,
      });
      if (i == e.currentTarget.dataset.index) {
        that.setData({
          ['list[' + i + '].ischecked']: true,
        })
        wx.request({
          url: api + '/topics?tab='+that.data.list[i].tar,
          success: (res) => {
            that.setData({
              dataList: res.data.data
            })
            
            wx.hideLoading()
          }
        })
      }
    }
  },
  toDetail(e){
    wx.navigateTo({
      url: '../article/article?uid=' + e.currentTarget.dataset.uid
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: "正在加载..."
    });
    var that = this;
    this.setData({
      ['list[0].ischecked']: true
    });
    wx.request({
      url: api+'/topics',
      success: (res) => {
        that.setData({
          dataList: res.data.data
        });
        var time = that.data.dataList[1].last_reply_at;
        var replyTime = app.replyTime(time);
        var nowTime = app.getTime();
        wx.hideLoading();
        
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    setTimeout(function () {
      wx.stopPullDownRefresh()
    }, 1000);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})